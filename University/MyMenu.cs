﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University
{
    public class MyMenu
    {
        public delegate void MenuDelegate(object obj);
        private List<MenuItem> items = new List<MenuItem>();
        class MenuItem
        {
            public string MenuItemString;
            public event MenuDelegate Action;
            public void DoWork(object obj)
            {
                Action?.Invoke(obj);
            }
        }

        public void AddMenuItem(string menuItemStr, MenuDelegate d)
        {
            MenuItem item = items.FirstOrDefault(x => x.MenuItemString.CompareTo(menuItemStr) == 0);

            if (item != null)
            {
                item.Action += d;
            }
            else
            {
                MenuItem newItem = new MenuItem { MenuItemString = menuItemStr };
                newItem.Action += d;
                items.Add(newItem);
            }
        }
        public void Run(object obj)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            int counter = 0;
            Console.Clear();
            while (true)
            {
                for (int i = 0; i < items.Count; ++i)
                {
                    if (counter == i)
                    {
                        Console.Write("===>");
                    }
                    else
                    {
                        Console.Write("    ");
                    }
                    Console.WriteLine(items[i].MenuItemString);
                }
                switch (Console.ReadKey(false).Key)
                {
                    case ConsoleKey.UpArrow:
                        --counter;
                        break;
                    case ConsoleKey.DownArrow:
                        ++counter;
                        break;
                    case ConsoleKey.Enter:
                        items[counter].DoWork(obj);
                        break;
                }
                if (counter < 0)
                {
                    counter = items.Count - 1;
                }
                if (counter >= items.Count)
                {
                    counter = 0;
                }
                Console.Clear();
            }
        }
    }
}

