﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProcess;
using System.Threading;

namespace University
{
    static class AddToMenu
    {
        private static List<string> information = new List<string>();
        private static int mark, index;

        public static void Add()
        {
            MyMenu menu = new MyMenu();
                menu.AddMenuItem("Поставить оценку", x =>
                {
                    Console.Clear();
                    Add(AllUniversity.lectors);
                });
            AddToMenu.Students();
            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить работу...");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Загрузка...");
            Thread.Sleep(1000);
            menu.Run(new object());
        }
        public static void Add(List<Lector> list)
        {
            //Console.Clear();
            MyMenu menuLector = new MyMenu();
            foreach (Lector lector in list)
            {
                menuLector.AddMenuItem(lector.ToString(), x =>
               {
                   index = lector.index;
                   Add(lector.yourSubjects);
                   Console.Clear();
               });
            }
            Console.WriteLine("Переход к выбору лектора");
            Thread.Sleep(1000);
            menuLector.Run(new object());
        }

        public static void Add(List<Subjects> list)
        {
            Console.Clear();
            MyMenu menuSubj = new MyMenu();

            foreach (Subjects subj in list)
            {
                menuSubj.AddMenuItem(subj.ToString(), x =>
                {
                    information.Add(subj.ToString());
                    Add(AllUniversity.groups);
                });
            }
            Console.WriteLine("Переход к выбору предмета");
            Thread.Sleep(1000);
            menuSubj.Run(new object());
        }
        public static void Add(List<Groups> list)
        {
            Console.Clear();
            MyMenu menuGroups = new MyMenu();

            foreach (Groups groups in list)
            {
                menuGroups.AddMenuItem(groups.ToString(), x =>
                {
                    information.Add(groups.ToString());
                    Add(groups.group);
                });
            }
            Console.WriteLine("Переход к выбору группы");
            Thread.Sleep(1000);
            menuGroups.Run(new object());
        }

        public static void Add(List<Student> list)
        {
            Console.Clear();
            MyMenu menuStudents = new MyMenu();

            foreach (Student student in list)
            {
                menuStudents.AddMenuItem(student.ToString(), x =>
                {
                    information.Add(student.lastName);
                    information.Add(student.firstName);
                    Add(AllUniversity.allMarks);
                });
            }
            Console.WriteLine("Переход к выбору студента");
            Thread.Sleep(1000);
            menuStudents.Run(new object());
        }

        public static void Add(List<Marks> list)
        {
            Console.Clear();
            Console.WriteLine("Выберите оценку");
            MyMenu menuMarks = new MyMenu();

            foreach (Marks marks in list)
            {
                menuMarks.AddMenuItem(marks.ToString(), x =>
                {
                    Console.Clear();
                   mark = marks.Mark;
                    AllUniversity.lectors[index].RateStudents(information[1], information[2], information[3], information[0], mark);
                    information.Clear();
                    Console.WriteLine("Выставление оценки.");
                    Thread.Sleep(1000);
                    Add();
                });
            }
            menuMarks.Run(new object());
            
        }

        public static void Students()
        {
            Console.Clear();
            foreach (Student element in AllUniversity.students)
            {
                Console.WriteLine(element);
                Console.WriteLine("Оценки:");
                foreach (KeyValuePair<Subjects, Marks> marks in element.marks)
                {
                    Console.WriteLine(marks.Key + ": " + marks.Value + ".");
                }

                Console.WriteLine();
            }
        }
    }
}
