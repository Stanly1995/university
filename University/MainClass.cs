﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProcess;

namespace University
{
    class MainClass
    {
        static void Main(string[] args)
        {
            //Filling out information about students, lecturers, subjects and scales of assessments.
            int indexOfStudent = 0;
            int indexOfLector = 0;
            for (int i=1; i<6; i++)
            {
                AllUniversity.allMarks.Add(new Marks(i));
            }
            AllUniversity.subj.Add(new Subjects("Программирование"));
            AllUniversity.subj.Add(new Subjects("Тестировка"));
            AllUniversity.AddGroup("Программисты");
            AllUniversity.AddGroup("Тестировщики");
            AllUniversity.students.Add(new Student("Медведев", "Александр", 22, indexOfStudent));
            indexOfStudent++;
            AllUniversity.students.Add(new Student("Пивовар", "Александр", 25, indexOfStudent));
            indexOfStudent++;
            AllUniversity.students.Add(new Student("Иванов", "Александр", 20, indexOfStudent));
            AllUniversity.students[0].AddToGroup("Программисты");
            AllUniversity.students[1].AddToGroup("Программисты");
            AllUniversity.students[2].AddToGroup("Тестировщики");
            AllUniversity.lectors.Add(new Lector("Кобельчак", "Александр", indexOfLector));
            indexOfLector++;
            AllUniversity.lectors.Add(new Lector("Прокофьев", "Олег", indexOfLector));
            AllUniversity.lectors[0].AddSubject("Программирование");
            AllUniversity.lectors[1].AddSubject("Тестировка");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.DarkBlue;         
            AddToMenu.Add();
            Console.ReadKey();
        }
    }
}
