﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public class Student : Human
    {
        Dictionary <Subjects , Marks> privateMarks = new Dictionary<Subjects, Marks>();
        string privateGroupName=string.Empty;

        public Dictionary<Subjects, Marks> marks
        {
            get { return privateMarks; }
            set { privateMarks = value; }
        }
        public string groupName
        {
            get { return privateGroupName; }
            set { privateGroupName = value; }
        }
        public Student() : base()
        {
        }
        public Student(string lastName, string firstName, int age, int indexOf) : base(lastName, firstName, age, indexOf)
        {

        }
        public Student(string lastName, int age, int indexOf) : base(lastName, age, indexOf)
        {

        }
        public Student(string lastName, string firstName, int indexOf) : base(lastName, firstName, indexOf)
        {

        }

        public Student(string lastName, int indexOf) : base(lastName, indexOf)
        {

        }

        public void AddToGroup(string groupName)//Adding a student to the group.
        {
            bool groupMarker = false;
            foreach(Groups element in AllUniversity.groups)
            {
                if (element.name.Equals(groupName))
                {
                    element.group.Add(AllUniversity.students[index]);
                    this.groupName = groupName;
                    groupMarker = true;
                    break;
                }
                if (groupMarker == false)
                    Console.WriteLine("Нет такой группы.");
            }
        }
        public override string ToString()
        {
            if (lastName == "")
                return "Введите фамилию";
            else
            {
                StringBuilder str = new StringBuilder();
                str.Append("Студент: " + lastName);
                if (firstName != "")
                    str.Append(" " + firstName);
                if (age>0)
                    str.Append(", возраст: " + age);
                str.Append(".");
                if (!groupName.Equals(string.Empty))
                    str.Append(" Группа: " + groupName + ".");
                return str.ToString();
            }
        }
    }
}
