﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public class Subjects
    {
        string privateSubject;
        public string subject
        {
            get { return privateSubject; }
        }
        public Subjects(string subject)
        {
            privateSubject = subject;
        }

        public override string ToString()
        {
            return subject;
        }
    }
}
