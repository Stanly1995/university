﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public static class AllUniversity//The class where all the lists are stored and you can add a group.
    {
        public static List<Groups> groups = new List<Groups>();
        public static List<Student> students = new List<Student>();
        public static List<Lector> lectors = new List<Lector>();
        public static List<Subjects> subj = new List<Subjects>();
        public static List<Marks> allMarks = new List<Marks>();
        public static void AddGroup(string newGroup)
        {
            bool groupMarker = false;
            foreach (Groups element in groups)
            {
                if (element.name.Equals(newGroup))
                {
                    Console.WriteLine("Такая группа уже есть");
                    groupMarker = true;
                    break;
                }
            }
            if (groupMarker == false)
            {
                groups.Add(new Groups(newGroup));
            }
            groupMarker = false;
        }
    }
}
