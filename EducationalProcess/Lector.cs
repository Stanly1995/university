﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public class Lector: Human//Class for lecturers.
    {
        List<Subjects> privateYourSubjects = new List<Subjects>();
        public List<Subjects> yourSubjects//A list of subjects that the lecturer reads.
        {
            get { return privateYourSubjects; }
            set { privateYourSubjects = value; }
        }
        public Lector() : base()
        {
        }
        public Lector(string lastName, string firstName, int age, int indexOf) : base(lastName, firstName, age, indexOf)
        {

        }
        public Lector(string lastName, int age, int indexOf) : base(lastName, age, indexOf)
        {

        }
        public Lector(string lastName, string firstName, int indexOf) : base(lastName, firstName, indexOf)
        {

        }

        public Lector(string lastName, int indexOf) : base(lastName, indexOf)
        {

        }
        public override string ToString()
        {
            if (lastName == "")
                return "Введите фамилию";
            else
            {
                StringBuilder str = new StringBuilder();
                str.Append("Лектор: " + lastName);
                if (firstName != "")
                    str.Append(" " + firstName);
                if (age>0)
                    str.Append(", возраст: " + age);
                str.Append(".");
                return str.ToString();
            }
        }

        public void AddSubject(string subjSome)//Adding more subjects that the lecturer reads.
        {
            foreach (Subjects element in AllUniversity.subj)
            {
                if (element.subject.Equals(subjSome))
                {
                    if (yourSubjects.Count > 0)
                    {
                        for (int i=0; i<yourSubjects.Count;i++)
                        {
                            if (!yourSubjects[i].subject.Equals(subjSome))
                            {
                                yourSubjects.Add(element);
                            }
                        }
                    }
                    else
                        yourSubjects.Add(element);
                }
            }
        }

        public void RateStudents(string groupName, string lastName, string firstName, string subject, int mark)//Evaluation of subjects.
        {
            foreach (Subjects sub in yourSubjects)
            {
                if (sub.subject.Equals(subject))
                {
                    foreach (Groups group in AllUniversity.groups)
                    {
                        if (group.name == groupName)
                        {
                            foreach (Student student in AllUniversity.students)
                            {
                                if (student.lastName == lastName)
                                {
                                    if (student.firstName == firstName)
                                    {
                                        foreach (Subjects someSubj in AllUniversity.subj)
                                        {
                                            if (someSubj.subject.Equals(subject))
                                            {
                                                foreach (Marks someMark in AllUniversity.allMarks)
                                                {
                                                    if (someMark.Mark.Equals(mark))
                                                    {
                                                        if (student.marks.Count > 0)
                                                        {
                                                            foreach (KeyValuePair<Subjects, Marks> keyValue in student.marks.ToArray())
                                                            {
                                                                if (!keyValue.Key.Equals(subject) && student.marks.ContainsKey(someSubj))
                                                                {
                                                                    student.marks.Remove(someSubj);
                                                                    student.marks.Add(someSubj, someMark);
                                                                }
                                                                else if (!keyValue.Key.Equals(subject) && !student.marks.ContainsKey(someSubj))
                                                                {
                                                                    student.marks.Add(someSubj, someMark);
                                                                }
                                                            }
                                                        }
                                                        else
                                                            student.marks.Add(someSubj, someMark);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
