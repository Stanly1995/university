﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public class Marks //Class for ratings.
    {
        int privateMark;
        public int Mark
        {
            get { return privateMark; }
        }
        public Marks(int mark)
        {
            privateMark = mark;
        }

        public override string ToString()
        {
            string mark = string.Empty;
            switch (Mark)
            {
                case 1:
                    mark = " - неудовлетворительно";
                    break;

                case 2:
                    mark = " - неудовлетворительно";
                    break;

                case 3:
                    mark = " - удовлетворительно";
                    break;

                case 4:
                    mark = " - хорошо";
                    break;

                case 5:
                    mark = " - отлично";
                    break;
            }
            return (Mark + mark);
        }
    }
}
