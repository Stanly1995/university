﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public class Groups// class for groups with students;
    {
        string privateName;
        List<Student> privateGroup = new List<Student>();
        public List<Student> group
        {
            get { return privateGroup; }
            set { privateGroup = value; }
        }
        public string name
        {
            get { return privateName; }
        }

        public Groups(string name)
        {
            privateName = name;
        }

        public override string ToString()
        {
            return privateName;
        }

    }
}
