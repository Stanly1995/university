﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProcess
{
    public class Human //A parent class for people.
    {
        string privateFirstName;
        string privateLastName;
        int privateAge;
        int privateIndex;

        public string firstName
        {
            get{return privateFirstName;}
        }
        public string lastName
        {
            get { return privateLastName; }
        }
        public int age
        {
            get { return privateAge; }
        }
        public int index
        {
            get { return privateIndex; }
        }

        public Human()
        {
        }
        public Human(string lastName, string firstName, int age, int indexOf)
        {
            privateFirstName = firstName;
            privateLastName = lastName;
            if (age>0)
                privateAge = age;
            privateIndex = indexOf;
        }
        public Human(string lastName, int age, int indexOf)
        {
            privateFirstName = "";
            privateLastName = lastName;
            if (age > 0)
                privateAge = age;
            privateIndex = indexOf;
        }
        public Human(string lastName, string firstName, int indexOf)
        {
            privateFirstName = firstName;
            privateLastName = lastName;
            privateAge = 0;
            privateIndex = indexOf;
        }

        public Human(string lastName, int indexOf)
        {
            privateFirstName = "";
            privateLastName = lastName;
            privateAge = 0;
            privateIndex = indexOf;
        }
    }
}
